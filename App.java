import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class App  {
    public static void main(String[] args) throws Exception {
        
        //Agent
        URL urlStaff = new URL("https://gitlab.com/Noxia654/MSPR-DEV/-/raw/main/staff.txt");
        HttpURLConnection conStaff = (HttpURLConnection) urlStaff.openConnection();   
        conStaff.setRequestMethod("GET");
        BufferedReader readerStaff = null;
        readerStaff = new BufferedReader(new InputStreamReader(urlStaff.openStream()));
        String lineStaff =  readerStaff.lines().collect(Collectors.joining("</br>"));
        List<String> agents = Arrays.asList(lineStaff.split("</br>"));

        String indexHml="<HTML><head><head><script src='Jquery.js'></script></head><script src='https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js' integrity='sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo' crossorigin='anonymous'></script><link rel='stylesheet' href='https://cdn.jsdelivr.net/npm/bootstrap@4.4.1/dist/css/bootstrap.min.css' integrity='sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh' crossorigin='anonymous'><script src='https://cdn.jsdelivr.net/npm/bootstrap@4.4.1/dist/js/bootstrap.min.js' integrity='sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6' crossorigin='anonymous'></script></head><BODY><div ><div class='row'><div class='col-12 text-center mt-5 pt-5'><img src='https://gitlab.com/Noxia654/MSPR-DEV/-/raw/main/logo.png' style='width: 10%;'></div><div class='col-12 text-center mt-4'><input id='searchBar' style='width: 300px;height: 40px;text-align: center;border-radius: 100px;border-color: #659224;'></div><div class='col-12 text-center mt-3'><button id='button'  style='background-color:#379EC1;border-radius: 5px;'>Rechercher</button></div><div class='col-12 text-center mt-3'><div>Liste des codes agents :</div>";
        for (String string : agents) {
            indexHml+="<div class='mt-3'><a href='" + string+".html'>"+string+"</a></div>";
        }
        indexHml+=" </div></div></div></BODY></HTML><script>$(document).on('click', '#button', function () {var value = $('#searchBar').val();console.log(value); window.location.href = value + '.html';});</script>";
        // System.out.println(indexHml);
        //Création et ecriture du fichier
        File indexFile = new File("/var/lib/jenkins/workspace/transfere_fichier/index.html");
        indexFile.createNewFile();
        FileWriter fwIndex = new FileWriter(indexFile.getAbsoluteFile());
        BufferedWriter bwIndex = new BufferedWriter(fwIndex);
        bwIndex.write(indexHml);
        bwIndex.close();
        for (String id : agents) 
        {
       
            //Agent
            URL url = new URL("https://gitlab.com/Noxia654/MSPR-DEV/-/raw/main/"+id+".txt");
            HttpURLConnection con = (HttpURLConnection) url.openConnection();   
            con.setRequestMethod("GET");
            BufferedReader reader = null;
            reader = new BufferedReader(new InputStreamReader(url.openStream()));
            String line =  reader.lines().collect(Collectors.joining("</br>"));
            List<String> items = Arrays.asList(line.split("</br>"));
            String nom = items.get(0);
            String prenom = items.get(1);
            String poste = items.get(2);
            String password = items.get(4);

            //liste
            URL urlListe = new URL("https://gitlab.com/Noxia654/MSPR-DEV/-/raw/main/liste.txt");
            HttpURLConnection conListe = (HttpURLConnection) urlListe.openConnection();   
            conListe.setRequestMethod("GET");
            BufferedReader readerListe = null;
            readerListe = new BufferedReader(new InputStreamReader(urlListe.openStream()));
            String lineListe =  readerListe.lines().collect(Collectors.joining("</br>"));
            List<String> itemsListe = Arrays.asList(lineListe.split("</br>"));
            
            String equipmentList = "";
            for (String item : itemsListe) {
                List<String> aa = Arrays.asList(item.split("\\s+"));
                boolean ans = items.contains(aa.get(0));
                
                equipmentList+="<div> "+aa.get(0)+" <input type='checkbox'disabled='disabled'";
                if (ans)
                {
                    equipmentList+=" checked='checked'";
                    // System.out.println("item : "+aa);
                }
                equipmentList+=" ></input></div></br>";
            }

            //conversion en HTML
            String html = "<HTML><head><script src='https://code.jquery.com/jquery-3.4.1.slim.min.js' integrity='sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n' crossorigin='anonymous'></script><script src='https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js' integrity='sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo' crossorigin='anonymous'></script><link rel='stylesheet' href='https://cdn.jsdelivr.net/npm/bootstrap@4.4.1/dist/css/bootstrap.min.css' integrity='sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh' crossorigin='anonymous'><script src='https://cdn.jsdelivr.net/npm/bootstrap@4.4.1/dist/js/bootstrap.min.js' integrity='sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6' crossorigin='anonymous'></script></head><BODY><div ><div class='row'><div class='col-6'><div class='ml-3 mt-3'>";
            html+= prenom + nom + " : " + poste;
            html+= "</div></div><div class='col-6 text-right'><img class='mr-3 mt-3' style='width: 50%;' src='https://gitlab.com/Noxia654/MSPR-DEV/-/raw/main/";
            html+=id+".jpg'></div></div><div class='row'><div class='col-12 text-center mt-3'>"+equipmentList+"</div></div></div></BODY></HTML>";
            
            //Création et ecriture du fichier
            File file = new File("/var/lib/jenkins/workspace/transfere_fichier/"+id+".html");
            file.createNewFile();
            FileWriter fw = new FileWriter(file.getAbsoluteFile());
            BufferedWriter bw = new BufferedWriter(fw);
            bw.write(html);
            bw.close();

        }
    }
}